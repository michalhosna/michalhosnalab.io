#!/usr/bin/env bash
set -euox pipefail
IFS=$'\n\t'
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

docker run --rm -v "$DIR:$DIR" -w "$DIR" jbergknoff/sass css/style.scss css/style.css

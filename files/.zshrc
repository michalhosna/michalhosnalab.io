source /usr/share/zsh-antigen/antigen.zsh

antigen use oh-my-zsh

antigen bundle git
antigen bundle lein
antigen bundle command-not-found
antigen bundle docker
antigen bundle gnu-utils
antigen bundle docker-compose
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle mafredri/zsh-async

antigen theme robbyrussell

antigen apply

# Show exit code of all (piped) commands in RPROMPT
precmd_pipestatus() {
  PIPESTATUS="${(j.|.)pipestatus}"
}
add-zsh-hook precmd precmd_pipestatus
# /Show exit code of all (piped) commands in RPROMPT

# colorize manpages
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

export HISTSIZE=100000
export SAVEHIST=100000

export EDITOR='/usr/bin/vim'

PS1=$'[%{$fg_bold[red]%}%n%{$reset_color%}@%{$fg_bold[green]%}%M%{$reset_color%}%{$fg_bold[red]%}%{$reset_color%} %{$fg_bold[blue]%}%d%{$reset_color%} $PIPESTATUS ]% # '

HYPHEN_INSENSITIVE="true"
COMPLETION_WAITING_DOTS="true"
HIST_STAMPS="yyyy-mm-dd"

alias gs="git status"
alias gwip="git add -A && git commit -m '[WIP]'"
alias gback='git reset --soft HEAD~'
alias glof='git log --oneline --graph --decorate --all'
alias gre='git rebase --autosquash --interactive --preserve-merges'
alias gre-m='git fetch && git rebase --autosquash --interactive --preserve-merges origin/master'

alias dc="docker-compose"

alias ll="ls -la"

# Macos Alt
bindkey "^[^[[C" forward-word
bindkey "^[^[[D" backward-word

export LC_ALL=en_US.UTF-8
